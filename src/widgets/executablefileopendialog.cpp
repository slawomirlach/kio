/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2014 Arjun A.K. <arjunak234@gmail.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "executablefileopendialog_p.h"

#include <QCheckBox>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include <KLocalizedString>

ExecutableFileOpenDialog::ExecutableFileOpenDialog(ExecutableFileOpenDialog::Mode mode, QWidget *parent)
    : QDialog(parent)
{
    QLabel *label = new QLabel(i18n("What do you wish to do with this file?"), this);

    m_dontAskAgain = new QCheckBox(this);
    m_dontAskAgain->setText(i18n("Do not ask again"));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel, this);

    QPushButton *openButton;
    if (mode == OpenOrExecute) {
        openButton = new QPushButton(i18n("&Open"), this);
        openButton->setIcon(QIcon::fromTheme(QStringLiteral("document-preview")));
        buttonBox->addButton(openButton, QDialogButtonBox::AcceptRole);
    }

    QPushButton *executeButton = new QPushButton(i18n("&Execute"), this);
    executeButton->setIcon(QIcon::fromTheme(QStringLiteral("system-run")));
    buttonBox->addButton(executeButton, QDialogButtonBox::AcceptRole);

    buttonBox->button(QDialogButtonBox::Cancel)->setFocus();

    QVBoxLayout *layout = new QVBoxLayout(this);

    QFrame *comboSelPar = new QFrame();
    QHBoxLayout *comboSelLay = new QHBoxLayout(comboSelPar);
#ifdef __linux__
    QLabel *hintAppTypeSelector = new QLabel(i18n("Select communication way needed by this app"));
    QComboBox *messagesHandling = new QComboBox();

    messagesHandling->addItem(i18n("Ignore debug messages from app and this app do not request information from user (GUI APP)"));
    messagesHandling->addItem(i18n("Show messages from app in separate window and allow to input messages into the same window (console app)"));
    messagesHandling->addItem(i18n("Guess (you first run this app and do not be sure)"));

    this->appDetectionSelection = messagesHandling;
    comboSelLay->addWidget(hintAppTypeSelector);
    comboSelLay->addWidget(messagesHandling);
#endif
    layout->addWidget(label);
    layout->addWidget(comboSelPar);
    layout->addWidget(m_dontAskAgain);
    layout->addWidget(buttonBox);

    if (mode == OnlyExecute) {
        connect(executeButton, &QPushButton::clicked, this, [=] {
            done(ExecuteFile);
        });
    } else if (mode == OpenAsExecute) {
        connect(executeButton, &QPushButton::clicked, this, [=] {
            done(OpenFile);
        });
    } else {
        connect(openButton, &QPushButton::clicked, this, [=] {
            done(OpenFile);
        });
        connect(executeButton, &QPushButton::clicked, this, [=] {
            done(ExecuteFile);
        });
    }
    connect(buttonBox, &QDialogButtonBox::rejected, this, &ExecutableFileOpenDialog::reject);
}

ExecutableFileOpenDialog::ExecutableFileOpenDialog(QWidget *parent)
    : ExecutableFileOpenDialog(ExecutableFileOpenDialog::OpenOrExecute, parent)
{
}

bool ExecutableFileOpenDialog::isDontAskAgainChecked() const
{
    return m_dontAskAgain->isChecked();
}
