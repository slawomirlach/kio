/*
 *    PlasmaAppKindDetector  - Automatically detect program type and allow to spawn an terminal.
 *    Copyright (C) 2022  Lach Sławomir <slawek@lach.art.pl>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <poll.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
int main(int argc, char **argv)
{
    struct termios termios_p;
    struct pollfd pollfd[2];
    int console;

    if (argc != 2) {
        return 1;
    }

    console = atoi(argv[1]);
    if (-1 == console)
        return 1;

    tcgetattr(console, &termios_p);
    memset(&termios_p, 0, sizeof(termios_p));
    termios_p.c_lflag |= ICANON;
    termios_p.c_cc[VMIN] = 1;
    tcsetattr(console, TCSANOW, &termios_p);
    pollfd[0].fd = console;
    pollfd[0].events = POLLIN | POLLHUP;
    pollfd[0].revents = 0;
    pollfd[1].fd = 0;
    pollfd[1].events = POLLIN;
    pollfd[1].revents = 0;

    while (0 < poll(pollfd, 2, -1)) {
        if (POLLIN == (pollfd[0].revents & POLLIN)) {
            int count;
            char buffer[1024];

            if (0 < (count = read(console, buffer, 1024))) {
                write(1, buffer, count);
            }
        }

        if (POLLHUP == (pollfd[0].revents & POLLHUP) || POLLERR == (pollfd[0].revents & POLLERR)) {
            int count;
            char buffer[1024];

            while (0 < (count = read(console, buffer, 1024))) {
                write(1, buffer, count);
            }
            write(1, "\nPROGRAM TERMINATES\n", sizeof("\nPROGRAM TERMINATES\n") - 1);
            while (1)
                sleep(2000);
        }
        if (POLLIN == (pollfd[1].revents & POLLIN)) {
            int count;
            char buffer[1024];

            if (0 < (count = read(0, buffer, 1024))) {
                write(console, buffer, count);
            }
        }

        // pollfd[0].revents = 0;
        // pollfd[1].revents = 0;
    }
}
