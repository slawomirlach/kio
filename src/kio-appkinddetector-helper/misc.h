/*
 *    PlasmaAppKindDetector  - Automatically detect program type and allow to spawn an terminal.
 *    Copyright (C) 2022  Lach Sławomir <slawek@lach.art.pl>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <KNotification>
#include <QSocketNotifier>
#include <QTimer>

enum resolution_type { PENDING, GUI, CONSOLE, OTHER };

class FDWatcher : public QSocketNotifier
{
    Q_OBJECT

public:
    bool (*callback)(int fd, intptr_t data);
    intptr_t data;

    FDWatcher(int fd, QSocketNotifier::Type type)
        : QSocketNotifier(fd, type){};

public Q_SLOTS:

    void fired(QSocketDescriptor socket, QSocketNotifier::Type type);
};

class CheckAppAction : public QTimer
{
    Q_OBJECT
public:
    struct data *data_;
public Q_SLOTS:
    void update(void);
};

class NotifyHandler : public QObject
{
    Q_OBJECT
public:
    struct data *data_;
public Q_SLOTS:
    void handle(unsigned int);
};

struct data {
    enum resolution_type resolution, selection;
    void *m_window, *app_type_window, *term_kind_window, *error_wnd;
    char *error_txt;
    pid_t program_id, error_hub_id;
    int proxy_ch, proxy_ch2, error_ch;

    int argc;
    char **argv;

    char maybe_console;

    NotifyHandler *notify_handler;
    KNotification *notification;
    CheckAppAction *resolution_timer;
};
