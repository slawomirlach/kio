/*
 *    PlasmaAppKindDetector  - Automatically detect program type and allow to spawn an terminal.
 *    Copyright (C) 2022  Lach Sławomir <slawek@lach.art.pl>
 *    
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *    
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 600
#define _GNU_SOURCE

#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>
#include <sys/inotify.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <sys/socket.h>
// #include <sys/un.h>
#include <sys/wait.h>


#include <QGuiApplication>
#include <QIcon>
#include <KLocalizedString>

#include "misc.h"


struct data;


#include <QSocketNotifier>
#include <KNotification>
#include <QtGui/QIcon>

void FDWatcher::fired(QSocketDescriptor socket, QSocketNotifier::Type type)
{
  
  if (!this->callback(this->socket(), this->data)) {
    
    delete this;
  }
}


static void test_all_gui(pid_t pid, struct data *data);
static bool test_gui(char *path, struct data *data);
static void run_console(void *mesh, void *data_);
static void real_resolution(struct data *);


static void process_ended_error(struct data *data)
{
  int error = errno;
  
  perror("Unable to check app type");
  if (EEXIST == error|| data->maybe_console) {
    
    if (data->resolution != GUI) {
    
      data->resolution = CONSOLE;
      real_resolution(data);
    }
  }
  
  if (NULL != data->resolution_timer) {
  
    delete data->resolution_timer;
    data->resolution_timer = nullptr;
  } 
}

static pid_t start_program(struct data *data)
{
  pid_t proxy, child;
  int shouldOpen;
  struct termios termios_p;
  bool ok = true;
  int fdm = posix_openpt(O_RDWR | O_NOCTTY);
  if (fdm < 0)
  {
    
    ok  = 0;
  }
  int rc;
  if (ok) {
    rc = grantpt(fdm);
    if (rc != 0)
    {
      
      ok = 0;
    }
  }
  
  rc = unlockpt(fdm);
  
  proxy = -1;
  if ((shouldOpen = tcgetattr (fdm, &termios_p)) < 0) {
    
    ok = 0;
  }
  else {
    memset(&termios_p,0,sizeof(termios_p));  
    termios_p.c_oflag |= ONLCR;
    termios_p.c_lflag &= ~ECHO;
    termios_p.c_cc[VMIN]  = 1;
    tcsetattr(fdm, TCSANOW, &termios_p);
  }
  char *slavename = ptsname(fdm);
  ok = ((NULL != ptsname(fdm)) && ok);
  
  if (ok) {

    int proxy_ch[2];
    int proxy_ch2[2];
    int error_ch[2];
    int error_hub_ch[2];
    pipe(proxy_ch);
    pipe(proxy_ch2);
    pipe(error_ch);
    pipe(error_hub_ch);
    if (0 == (proxy = fork())) {
      int len;
      DIR *dir = opendir("/proc/self/fd");
      if (NULL == dir) return false;
      
      close(error_ch[0]);
      
      struct dirent *item;
      
      while ((item = readdir(dir))) {
        
        len = snprintf(NULL, 0, "/proc/self/fd/%s",  item->d_name) + 1;
        
        char *buffer2 = (char*) malloc(len);
        
        snprintf(buffer2, len, "/proc/self/fd/%s", item->d_name);
        
        if (test_gui(buffer2, data)) {
          
          
          close(atoi(item->d_name));
        }
        
        free(buffer2);
      }
      close(proxy_ch[0]);
      close(proxy_ch2[1]);
      
      
      // The program we want to ran
      if (0 == (child = fork())) {

        close(error_hub_ch[0]);
        int fd = open(slavename, O_RDWR);
        dup2(fd, 0);
        dup2(fd, 1);
        char *val = getenv("WXConsoleSpawner_SHOW_STDERR_IN_WINDOW");
        if (NULL != val && 0 == strcasecmp("TRUE", val))
          dup2(error_hub_ch[1], 2);
        else
          dup2(fd, 2);
        execvp(data->argv[1], &data->argv[1]);
        
        int error_type = errno;
        write(error_hub_ch[1], "Cannot start program:", sizeof("Cannot start program:") - 1);
        write(error_hub_ch[1], strerror(error_type), strlen(strerror(error_type)));
        write(error_hub_ch[1], "\n", sizeof("\n") - 1);
        exit(1);
      }
      
      // Error messages hub
      pid_t error_hub;
      if (0 == (error_hub = fork())) {
        
        int read_;
        int fd = open(slavename, O_RDWR);
        close(error_hub_ch[1]);
        
        char buffer[100];
        while (0 < (read_ = read(error_hub_ch[0], buffer, 100))) {
        
          write(error_ch[1], buffer, read_);
          write(fd, buffer, read_);
        }
        exit(0);
      }
      write(proxy_ch[1], &child, sizeof(pid_t));
      write(proxy_ch[1], &error_hub, sizeof(pid_t));
      
      struct pollfd pollfd;
      
      pollfd.fd = fdm;
      pollfd.events = POLLIN | POLLOUT;
      
      do {
      
        pollfd.revents = 0;
        poll(&pollfd, 1, -1);
        
      } while ((pollfd.revents && POLLIN != POLLIN)
        ||    pollfd.revents &&  POLLOUT != POLLOUT 
      );
      
      write(proxy_ch[1], "OKI", sizeof("OKI"));
      // READ ORDER TO RUN TERMINAL
      char order[1];
      if (1 > read(proxy_ch2[0], order, 1)) {
      
        exit(0);
      }
      int pipefd[2];
      
      pipe(pipefd);
      
      if (0 == fork()) {
        
        close(pipefd[1]);
        dup2(pipefd[0], 0);
        dup2(proxy_ch[1], 2);
        execlp("sh", "sh", NULL);
        perror("Cannot start shell");
        exit(1);
      }

      
      int len2 = snprintf(NULL, 0, "%d", fdm) + 1;
      
      char buff_fdn[len2];
      
      snprintf(buff_fdn,len2, "%d", fdm);
      
      close(pipefd[0]);
      write(pipefd[1], "xdg-terminal", sizeof("xdg-terminal") - 1);
      write(pipefd[1], " \"", sizeof(" \"") - 1);
      write(pipefd[1], "WXConsoleSpawner_console_server", sizeof("WXConsoleSpawner_console_server") - 1);
      write(pipefd[1], " ", sizeof(" ") - 1);
      write(pipefd[1], buff_fdn, strlen(buff_fdn));
      write(pipefd[1], "\"", sizeof("\"") - 1);
      write(pipefd[1], "\n", sizeof("\n") - 1);
      wait(NULL);
      exit(1);
    }
    else {
      
      close(proxy_ch[1]);
      close(error_ch[1]);
      
      data->error_ch = error_ch[0]; 
      pid_t child_pid, error_hub_id;
      
      read(proxy_ch[0], &child_pid, sizeof(pid_t));
      read(proxy_ch[0], &error_hub_id, sizeof(pid_t));
      
      data->proxy_ch = proxy_ch[0];
      data->proxy_ch2 = proxy_ch2[1];
      data->error_hub_id = error_hub_id;
  
      return child_pid;
    }

  }

  return 0;
}

void NotifyHandler::handle(unsigned int event)
{
  if (1 == event) {
  
    run_console(NULL, this->data_);
  }

}

static void real_resolution(struct data *data)
{
  switch (data->resolution) {
    
    case PENDING:
      break;
    case CONSOLE:
     if (nullptr == data->notification) { 
      puts("autoresolution (Console)");
      data->notify_handler = new NotifyHandler();
      data->notify_handler->data_ = data;
      data->notification = new KNotification(QString::fromUtf8("kioDetectAppKind"));
      QIcon icon = QIcon::fromTheme(QString::fromUtf8("utilities-terminal"));
      data->notification->setText(i18n("The <i>%1</i> application sems to be console", QString::fromUtf8(data->argv[1])));
      data->notification->setPixmap(icon.pixmap(icon.actualSize(QSize(32, 32))));
      data->notification->setActions({i18n("Open Terminal Window")});
      QObject::connect(data->notification, QOverload<unsigned int>::of(&KNotification::activated), data->notify_handler, &NotifyHandler::handle);
      
      data->notification->sendEvent();
      
     }
      // We cannot remove timer, because GUI app could print something on console and next initialize GUI 
      break;
    case GUI:
      exit(0);
      break;
    case OTHER:
      break;
  }
}

void CheckAppAction::update(void)
{
  struct data *data = this->data_;
  
  if (NULL == data->resolution_timer) {
    
    return;
  }
  test_all_gui(data->program_id, data);
  real_resolution(data);
}


static void resolution(struct data *data)
{
  if (NULL == data->resolution_timer) {
  
    return;
  }
  test_all_gui(data->program_id, data);
  real_resolution(data);
}


static bool  error_msg(int fd, struct data *data)
{
  
  if (PENDING != data->selection) {
  
    return false;
  }
  
  char buffer[100];
  
  int count;
  int len = 0;
  
  if (NULL != data->error_txt) {
    
    len = strlen(data->error_txt);
  }
  int flags = fcntl(fd, F_GETFL, 0);
  fcntl(fd, F_SETFL, flags | O_NONBLOCK);
  while (0 < (count = read(fd, buffer, sizeof(buffer) - 1))) {
    
    buffer[count] = '\0';      
    data->error_txt = (char*) realloc(data->error_txt, count + len + 1);
    
    
    strcpy(&data->error_txt[len], buffer);
    len += count;
  }
  
  
  return false;
}

static bool  message(int fd, intptr_t data_)
{
  struct data *data = (struct data*) data_;
  (void) fd;
  
  
  data->maybe_console = true;
  if (PENDING == data->resolution
    || GUI  == data->resolution
  ) {
  
    return false;
  }
  data->resolution = CONSOLE;
  real_resolution(data);
  
  return false;
}

static void exit_from_app(void *mesh, struct data *data)
{
  (void) mesh;
  
  close(data->proxy_ch2);
  close(data->proxy_ch);
  close(data->error_ch);
  
  kill(data->error_hub_id, SIGTERM);
  
  if (NULL != data->resolution_timer)
  delete data->resolution_timer;
}

static void button_clicked_finilization(enum resolution_type selection, struct data *data)
{
  (void) selection;
  
  
  if (NULL != data->error_wnd) {
  
    if (NULL != data->error_txt) {
    
      free(data->error_txt);
      data->error_txt = NULL;
    }
    
    data->error_wnd = NULL;
  }
  
  exit_from_app(NULL, data);
  
}

static void run_console(void *mesh, void *data_)
{
  (void) mesh;
  
  char command = 'R';
  struct data *data = (struct data*) data_;
  
  write(data->proxy_ch2, &command, 1);
  
  button_clicked_finilization(CONSOLE, data);
}

static void app_is_gui(void *mesh, void *data_)
{
  (void) mesh;
  
  struct data *data = (struct data*) data_;
  
  button_clicked_finilization(GUI, data);
}

static void run_depending_on_res(void *mesh, void *data_)
{
 
  struct data *data = (struct data*) data_;
  
  switch (data->resolution) {
  
    case CONSOLE:
      run_console(mesh, data_);
      break;
    case GUI:
      app_is_gui(mesh, data_);
      break;
    case OTHER:
      break;
    case PENDING:
      
      break;
  }
  

}

static bool test_gui(char *path, struct data *data)
{

  (void) data;
   {
    
    struct stat stat_;
    if (-1 == lstat(path, &stat_)) {
    
      process_ended_error(data);
      return false;
    }
    
    if (S_ISLNK(stat_.st_mode)) {
    
      char buffer[4096];
      int a = readlink(path, buffer, sizeof(buffer));
      
      if (-1 == a) {
        
        process_ended_error(data);
        return false;
      }
      buffer[a] = '\0';
  
      if (0 == strncmp(buffer, "/dev/dri/", sizeof("/dev/dri/") - 1)) {
        
        return true;
      }
      else if (0 == strncmp("socket:[", buffer, sizeof("socket:[") - 1)) {
      
        
        char *searching_for1;
        
        int len = snprintf(NULL, 0, "/run/user/%d/%s", getuid(), getenv("WAYLAND_DISPLAY")) +1;
        
        searching_for1 = (char*) malloc(len);
        
        snprintf(searching_for1, len, "/run/user/%d/%s", getuid(), getenv("WAYLAND_DISPLAY")); 
        
        
        char *searching_for2;
        
        len = snprintf(NULL, 0, "/tmp/.X11-unix/X%s", getenv("DISPLAY") + 1) +1;
        
        searching_for2 = (char*) malloc(len);
        
        snprintf(searching_for2, len, "/tmp/.X11-unix/X%s", getenv("DISPLAY") + 1); 
        
        
        char *curr = &buffer[sizeof("socket:[")];
        
        while ('\0' != *curr && ']' != *curr) { ++curr; }
        
        if (']' != *curr) { return false;}
        
        *curr = '\0';
        int map = open("/proc/net/unix", O_RDONLY);
        
        if (-1 == map) {
        
          process_ended_error(data);
          return false;
        }
        char file_content[99999];
        
        int gathered = read(map, file_content, sizeof(file_content) - 1);
        close(map);
        file_content[gathered] = '\0';
        
        
        curr = file_content;
        
        while ('\0' != *curr && '\n' != *curr) {
        
          ++curr;
        }
        
        if ('\0' == *curr) return false;
        ++curr;
        
      
        
        while ('\0' != *curr) {
#define ommit  \
          \
          while (('\0' != *curr) && ('\n' != *curr) \
            &&  (' ' != *curr)) ++curr; \
           ;\
          if (' ' != *curr) { \
          \
            if ('\0' == *curr) { \
            \
              return false; \
            }  \
            \
            ++curr; \
            continue; \
          } \
          ++curr;
          
          ommit; 
          ommit; 
          ommit; 
          ommit; 
          ommit; 
          ommit; 
          
          char *start = curr; 
          ommit;
          
          *(curr-1) = '\0';
          
          if (0 != strcmp(start, &buffer[sizeof("socket:[") - 1])) {
          
            ommit;
            continue;
          }

#undef ommit
          
          start = curr;
          while ('\n' != *curr && '\0' != *curr) {
          
            ++curr;
          }
          
          if ('\0' == *curr) {
            
            return false; 
          }
          
          *curr = '\0';
          ++curr;
          
          
          if ('@' == *start) {
          
            ++start;
          }
         
          if (0 == strcmp(searching_for1, start) || 0 == strcmp(searching_for2, start)) {
          
            
            return true;
          }
        }
      }
    }
  }

  
  return false;
}



static void test_all_gui(pid_t pid, struct data *data)
{
  int len = snprintf(NULL, 0, "/proc/%d/fd", pid) + 1;
  
  char buffer[len];
  snprintf(buffer, len, "/proc/%d/fd", pid);
  DIR *dir = opendir(buffer);
  
  if (NULL == dir) {
   
    process_ended_error(data);
    return;
  }
  
  struct dirent *item;
  
  while ((item = readdir(dir))) {
  
    len = snprintf(NULL, 0, "/proc/%d/fd/%s", pid, item->d_name) + 1;
    
    char *buffer2 = (char*) malloc(len);
    
    snprintf(buffer2, len, "/proc/%d/fd/%s", pid, item->d_name);
    
    if (test_gui(buffer2, data)) {
    
      data->resolution = GUI;
      real_resolution(data);
      free(buffer2);
      return;
    }
    
    free(buffer2);
  }
  
  
  len = snprintf(NULL, 0, "/proc/%d/maps", pid) + 1;
  
  char *buffer2 = (char*) malloc(len);
  
  snprintf(buffer2, len, "/proc/%d/maps", pid);
  
  int fd = open(buffer2, O_RDONLY);
  
  if (-1 == fd) {
    
    process_ended_error(data);
    free(buffer2);
    return;
  }
  
  free(buffer2);
  
  char buffer3[99999];
  
  int gathered = read(fd, buffer3, sizeof(buffer3) - 1);
  close(fd);
  buffer3[gathered] = '\0';
  
  
  char *curr = buffer3;
  
  bool end = false;
  
  while (false == end) {
    char *next = curr;
    
    while ('\n' != *next
      &&   '\0' != *next
    )  {
      
      if ('/' == *next) {
      
        curr = next;
        ++curr;
      }
      
      ++next;
    }
    
    
    if ('\0' == *next) {
    
      end = true;
    }
    
    *next = '\0';
    ++next;
    
    if (0 == strncmp("libX11.so", curr, sizeof("libX11.so") - 2)
      ||0 == strncmp("libxcb.so", curr, sizeof("libX11.so") - 2)) {
    
       data->resolution = GUI;
       real_resolution(data);
       return;
      }
      
    curr = next;
  }
  
  
  if (true == data->maybe_console) {
    data->resolution = CONSOLE;
    real_resolution(data);
  }
  
}

int main(int argc, char **argv)
{
  struct data data;
  QGuiApplication *application = new QGuiApplication(argc, argv);
  
  puts(application->applicationName().toStdString().c_str());
  if (2 > argc) {
  
    exit(1);
  }
  
  data.notification = nullptr;
  data.argc = argc;
  data.argv = argv;
  data.program_id = 0;
  data.resolution = PENDING;
  data.selection = PENDING;
  data.app_type_window = nullptr;
  data.error_wnd = NULL;
  data.error_txt = NULL;
  data.error_ch = -1;
  data.maybe_console = false;
  data.resolution_timer = NULL;
  
  char *val = getenv("WXConsoleSpawner_SHOW_STDERR_IN_WINDOW");

  

  data.program_id = start_program(&data);
  
  
  
  FDWatcher *watcher = new FDWatcher(data.proxy_ch, QSocketNotifier::Read);
  
  watcher->callback = message;
  watcher->data = (intptr_t)(void*)&data;
  
  watcher->connect(watcher, &QSocketNotifier::activated, watcher, &FDWatcher::fired);
  
  test_all_gui(data.program_id, &data);
  real_resolution(&data);
  
  data.resolution_timer =  new CheckAppAction();
  data.resolution_timer->data_ = &data;
  QObject::connect(data.resolution_timer, &QTimer::timeout, data.resolution_timer, &CheckAppAction::update);
  data.resolution_timer->start(1500);
  

  application->exec();
  
  return EXIT_SUCCESS;
}
