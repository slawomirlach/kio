project(kio-appkinddetector-helper)


add_subdirectory(server)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(ECM REQUIRED 5.85.0 NO_MODULE)
LIST(APPEND CMAKE_MODULE_PATH "${ECM_MODULE_PATH}")

# Find the QtWidgets library
find_package(Qt${QT_MAJOR_VERSION}Widgets)
find_package(Qt${QT_MAJOR_VERSION}Core)
find_package(KF5Notifications)
find_package(Qt${QT_MAJOR_VERSION}DBus)
find_package(extra-cmake-modules)
find_package(KF5I18n)

INCLUDE(ECMOptionalAddSubdirectory)

INCLUDE(KDEInstallDirs)

add_executable(kio-appkinddetector-helper main.cxx  misc.h )

find_package(KF5Notifications)
target_link_libraries(kio-appkinddetector-helper KF5::Notifications Qt${QT_MAJOR_VERSION}::Widgets Qt${QT_MAJOR_VERSION}::Core Qt${QT_MAJOR_VERSION}::DBus  KF5::I18n)


install(FILES kio-appkinddetector-helper.notifyrc DESTINATION ${KDE_INSTALL_KNOTIFYRCDIR} )

install(FILES org.kde.kio-appkinddetector-helper.desktop DESTINATION "${CMAKE_INSTALL_FULL_DATAROOTDIR}/applications" )

install(TARGETS  kio-appkinddetector-helper DESTINATION ${CMAKE_INSTALL_FULL_LIBEXECDIR} )
